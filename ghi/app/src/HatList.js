import React, { useEffect, useState } from 'react';
import Nav from './Nav';
import {Carousel, Card, Col, Row, Button, Dropdown, DropdownButton, ButtonGroup} from 'react-bootstrap'
import { Link, Routes, Route, useNavigate } from 'react-router-dom';
function HatList({hats}) {

    const [hatStates, setHats] = useState(hats);

    const DeleteHat = async(id) => {
        const deleteUrl = `http://localhost:8090/api/hat/${id}/`
        const fetchConfig = {
            method: "delete",
            
        }
        const response = await fetch(deleteUrl, fetchConfig)
        if (response.ok) {
            const data = await response.json()
            

            setHats(hatStates.filter((hatStates) => hatStates.id !== id));
        }
    }
    




    const navigate = useNavigate();


    const navigateNew = () => {
    // 👇️ navigate to /
    navigate('/hats/new');
    };
    
    if ({hats} === undefined) {
        return null;
    }

    return (
        <React.Fragment>
            <h1 className="text-center">Eat my hat</h1>
            <div className="container-fluid">
            <Carousel>
                {hatStates && hatStates.map(hat => {
                    return (
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src={hat.pic_url}
                            alt="First slide"
                            />
                            <Carousel.Caption>
                            <h3>{hat.fabric}</h3>
                            <p>{hat.color}</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })}
            </Carousel>
            <Row xs={1} md={4} className="g-4">
            {hatStates && hatStates.slice(0,3).map(hat => {
                return(
                    <Col key={hat.id}>
                        <Card key={hat.id}>
                            <Card.Img variant="top" src={hat.pic_url} />
                            <Card.Body key={hat.id}>
                            <Card.Title key={hat.id}>{hat.name}</Card.Title>
                            <Card.Text>
                                Fabric: {hat.fabric}
                            </Card.Text>
                            <Card.Text>
                                Color: {hat.color}
                            </Card.Text>
                            <Card.Text>
                                Style: {hat.style_name}
                            </Card.Text>
                            <Card.Text>
                                Location: {hat.location.closet_name}
                            </Card.Text>
                            </Card.Body>
                            <>
                                {['Dark'].map(
                                    (variant) => (
                                    <DropdownButton
                                        as={ButtonGroup}
                                        key={variant}
                                        id={`dropdown-variants-${variant}`}
                                        variant={variant.toLowerCase()}
                                        menuVariant="dark"
                                        title="More Info"
                                        className="super-colors"
                                    >   
                                        <Dropdown.Item onClick={navigateNew} eventKey="1">Create</Dropdown.Item>
                                        <Dropdown.Divider />
                                        <Dropdown.Item onClick={() => DeleteHat(hat.id)} value={hat.id} eventKey="4">Delete</Dropdown.Item>
                                    </DropdownButton>
                                    ),
                                )}
                                </>
                        </Card>
                    </Col>
                )})}
            </Row>
        </div>
    </React.Fragment>
    )
}
export default HatList;