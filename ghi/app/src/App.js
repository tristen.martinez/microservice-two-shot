import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import Nav from './Nav';
import HatList from './HatList';
import HatCreate from './HatCreate';


function App({hats, shoes}) {
  if ({shoes} === undefined) {
    return null;
  }
  if ({hats} ===undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route path="" element={<HatList {...hats}/>}/>
            <Route path="new" element ={<HatCreate/>}/>
          </Route>
          <Route path='shoes' element={<ShoesList shoes={shoes} />} />
          {/* <Route path='shoes'/> */}
          <Route path='shoes/new' element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
