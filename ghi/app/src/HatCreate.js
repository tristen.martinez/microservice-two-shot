import React from 'react'
class HatCreate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            fabric: "",
            color: "",
            pic_url: "",
            style_name: "",
            location: "",
            locations: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleFabricChange = this.handleFabricChange.bind(this)
        this.handleStyleChange = this.handleStyleChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handlePicUrlChange = this.handlePicUrlChange.bind(this)
        this.handleClosetChange = this.handleClosetChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations"
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.locations
        const createUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(createUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            const cleared = {
                name: "",
            fabric: "",
            color: "",
            pic_url: "",
            style_name: "",
            location: "",
            };
            this.setState(cleared)
        }
    }


    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
      }
    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style_name: value})
      }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
      }
    handlePicUrlChange(event) {
        const value = event.target.value;
        this.setState({pic_url: value})
      }
    handleClosetChange(event) {
        const value = event.target.value;
        this.setState({location: value})
      }
    
    

    render() {
        
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Room count" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleStyleChange} value={this.state.style_name} placeholder="Style" required type="text" name="style" id="style" className="form-control"/>
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handlePicUrlChange} value={this.state.pic_url} placeholder="Pic URL" required type="text" name="pic_url" id="pic_url" className="form-control"/>
                            <label htmlFor="pic_url">Pic URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleClosetChange} required id="closet" name="closet" className="form-select">
                            <option value="">Choose a closet</option>
                            {this.state.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
          );
    }
}
export default HatCreate;