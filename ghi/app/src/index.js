import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadShoesAndHats(){
  const hatResponse = await fetch('http://localhost:8090/api/hats/');
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  if (hatResponse.ok && shoeResponse.ok) {
    const hatData = await hatResponse.json()
    const shoeData = await shoeResponse.json()
    root.render(
      <App shoes={shoeData} hats={hatData}/>
    );
  } else {
    console.error(hatResponse || shoeResponse)
  }
}
loadShoesAndHats()