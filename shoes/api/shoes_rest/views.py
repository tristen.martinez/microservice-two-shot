import json
# from typing_extensions import Required
from django.http import JsonResponse
# from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
       'import_href',
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ['model_name', 'picture_url', 'id', 'manufacturer', 'color', 'bin']
    encoders = {'bin': BinVOEncoder()}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'id',
        
    ]

    encoder = {'bin': BinVOEncoder}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
            
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", 'GET'])
def api_show_shoes(request, pk):

    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse(
                {'Deleted': count > 0}
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
