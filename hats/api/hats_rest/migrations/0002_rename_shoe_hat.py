# Generated by Django 4.0.3 on 2022-06-15 19:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Shoe',
            new_name='Hat',
        ),
    ]
