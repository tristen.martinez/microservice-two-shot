from django.db import models

# Create your models here.


    
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return "Location VO ID : " + str(self.id) + " " + self.closet_name 


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color= models.CharField(max_length=50)
    pic_url = models.URLField()
    location = models.ForeignKey(LocationVO, related_name="closet", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50, null=True)

    def __str__(self):
        return "Hat ID " + str(self.id) + " " + "Name: " + self.name